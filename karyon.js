/**
 * @module karyon
 * (c) 2020-present Sergey Dirzu
 * @license MIT
**/
/*********************************************************  General Definitions
*/
const VERSION = '3.0.0';
const SYS = Symbol();
const MOUNTS = new Map;
const MIXINS = new WeakMap;
const ERRORS = new WeakSet;
const CONTEXTS = new WeakMap;
const INTERRUPTS = new Set;
const MULTICASTS = new Map;
let MNT, TASK;

/**************************************************************  Common Helpers
*/
const {isArray} = Array, {freeze, hasOwn, prototype: {toString}} = Object;

const isFn = any => typeof any === 'function';
const isText = any => typeof any === 'string';
const isBool = any => typeof any === 'boolean';
const isPrimitive = any => typeof (any ?? 1) !== 'object' && !isFn(any);
const isObject = any => any && toString.call(any) === '[object Object]';
const isContent = any => !isBool(any ?? false);

const fnOf = any => isFn(any) ? any : undefined;
const textOf = any => isContent(any) ? any.toString() : '';
const argsOf = ($ => (any = $) => any === $ ? $ : [any].flat())([]);
const thisOf = (self, $this = self, any) => self !== $this ? $this : any;

const mntOf = (ref, raw) => {
    const mnt = MOUNTS.get(ref);
    return raw || !mnt?.zombie ? mnt : undefined;
};

const targetOf = function (ref = null, raw, type) {
    this && ref && (ref = jail(resolve, this.context, ref) ?? null);
    return ref instanceof (type ?? Node) ? ref : mntOf(ref, raw)?.target ??
        (type && ref !== null ? fail(TypeError('invalid target')) : null);
};

const intOf = (i = 0, min = 0, max = Number.MAX_SAFE_INTEGER) => {
    try { i = BigInt(i); } catch { fail(TypeError('invalid integer')); }
    return i <= min ? min : i >= max ? max : Number(i);
};

const resolve = function (ref, ...args) {
    const context = this === TASK && TASK?.mnt.context;
    return !isFn(ref) ? ref : State.owns(ref) ? ref() : context
        ? ref.call(context, resolve.call(context, context.data))
        : ref.call(this, ...args);
};

const jail = (fn, $this, ...args) => {
    const task = TASK;
    try { return TASK = undefined, fn.call($this, ...args); }
    finally { TASK = task; }
};

const join = task => { (task.mnt.tasks ??= new Set).add(task); };
const free = task => { task.mnt.tasks?.delete(task) && task.kill(); };

const fuse = (({create, defineProperties, hasOwn}) => (self, props) => {
    const ref = props ?? self, cfg = {};
    for (const key in ref)
        hasOwn(ref, key) && (cfg[key] = {value: ref[key]});
    return props ? defineProperties(self, cfg) : create(null, cfg);
})(Object);

const fail = e => { throw e; }, noop = () => {}, $ = fuse({});

/*****************************************************************  DOM Helpers
*/
const NS = {
    html: 'http://www.w3.org/1999/xhtml',
    svg: 'http://www.w3.org/2000/svg',
    math: 'http://www.w3.org/1998/Math/MathML'
};

const create = ((DOC, XML) => (tag, ns, is = null) =>
    '#text' === tag ? DOC.createTextNode('') :
    '#comment' === tag ? DOC.createComment('') :
    '#cdata-section' === tag ? XML.createCDATASection('') :
    (is = is !== null && {is}, (ns ?? NS.html) === NS.html) ?
    is ? DOC.createElement(tag, is) : DOC.createElement(tag) :
    is ? DOC.createElementNS(ns, tag, is) : DOC.createElementNS(ns, tag)
)(document, new DOMParser().parseFromString('<xml/>', 'application/xml'));

const compile = (any, parent, before = null) => {
    if (isObject(any))
        any = mount(any);
    else if (isPrimitive(any))
        any = (any = textOf(any)) && document.createTextNode(any);
    else if (isFn(any))
        try { return compile(resolve.call(TASK, any), parent, before); }
        catch (e) { return raise(e), false; }
    else if (isArray(any))
        return !any.filter(any => !compile(any, parent, before)).length;
    else if (!(any instanceof Node))
        return false;
    return !!(any && parent.insertBefore(any, before));
};

const move = (target, before = null) => {
    if (target !== before && target.nextSibling !== before)
        target.parentNode.insertBefore(target, before), interrupt(target);
};

const detach = target => {
    try { target?.remove(); } catch {}
    broadcast('detach', target, interrupt);
};

export const insert = (types => (content, parent, before) => {
    const target = targetOf(content, true);
    parent = targetOf(parent), before = targetOf(before, true);
    !types.has(parent?.nodeType) && fail(TypeError('invalid parent'));
    return before && before.parentNode !== parent ? false :
        !target?.parentNode ? compile(content, parent, before) :
        target.parentNode === parent && !!move(target, before);
})(new Set([1, 9, 11]));

export const remove = ref => detach(targetOf(ref, true));

/**********************************************************  Components Manager
*/
export const Component = ((registry, refs) => fuse(function (name, options) {
    const builder = registry.get(name) ??
        fail(TypeError(`undefined component: ${name}`));
    const model = fuse({ns: 'html', is: name});
    jail(builder, this, model, options), refs.add(model);
    return model;
}, {
    define (name, builder, options) {
        !isFn(builder) && fail(TypeError('invalid builder'));
        const {class: cls, ...opt} = options ?? $;
        customElements.define(name, cls ?? class extends HTMLElement {}, opt);
        registry.set(name, builder);
    },
    owns: ref => refs.has(ref)
}))(new Map, new WeakSet);

/************************************************************  Emitters Manager
*/
export const Emitter = (() => {

    const emit = function ({emitter, find}, event, ...payload) {
        const queue = find(event);
        if (!queue)
            return void (event === 'error' && raise(emitter, ...payload));
        queue.id++, queue.renice?.(), queue.renice = null;
        for (const [handler, task] of queue.tasks) {
            if (task.waits > task.recursive || task.id === queue.id)
                continue;
            const {action, args, once} = task;
            const $this = thisOf(emitter, this, task.$this);
            task.waits++, once && off(queue, event, handler);
            try { jail(action, $this, ...args, ...payload); }
            catch (e) { raise(emitter, Error('handler exception'), e); }
            task.waits--;
        }
    };

    const on = function ({emitter, find, accept, ...cfg}, event, handler) {
        const action = fnOf(handler) ?? fnOf(handler?.action)
            ?? fail(TypeError('invalid handler'));
        const $this = thisOf(emitter, this, handler);
        if (accept && !jail(resolve, $this, accept, event))
            return;
        const queue = find(event) ?? {tasks: new Map, id: 0};
        if (!queue.events)
            try { queue.events = cfg.refs.set(event, queue); }
            catch { queue.events = cfg.keys.set(event, queue); }
        else if (queue.tasks.has(handler))
            return;
        const {[SYS]: env, priority, recursive, once, args} = handler;
        const nice = !env?.nice ? priority ? intOf(priority) : 0 :
            BigInt(env.nice) + BigInt(Number.MAX_SAFE_INTEGER);
        queue.tasks.set(handler, {
            $this, action, recursive: intOf(recursive), once,
            args: argsOf(args), nice, id: queue.id, waits: 0
        }), nice && (queue.renice = renice);
        return true;
    };

    const off = (queue, event, handler) => queue?.tasks.delete(handler)
        && (queue.tasks.size || queue.events.delete(event));

    const raise = (({error: log}) => (emitter, ...e) =>
        (emitter !== EMITTER ? EMITTER.emit : log)('error', ...e))(console);

    const renice = (compare => function () {
        this.tasks = new Map([...this.tasks].sort(compare));
    })(([,{nice: m}], [,{nice: n}]) => m > n ? -1 : m < n ? 1 : 0);

    return options => {
        const keys = new Map, refs = new WeakMap;
        const find = event => refs.get(event) ?? keys.get(event);
        const self = {emitter: fuse({
            emit (...args) { emit.call(this, self, ...args); },
            on (...args) { return !!on.call(this, self, ...args); },
            off: (event, handler) => !!off(find(event), event, handler),
            remove: event => !!find(event)?.events.delete(event),
            count: event => find(event)?.tasks.size ?? 0
        }), keys, refs, find, accept: fnOf(options?.accept)};
        return self.emitter;
    };
})();

const EMITTER = Emitter(), {on, off} = EMITTER;

/***********************************************************  Signaling Helpers
*/
const Signals = (init => fuse({
    mount: init(0x1, {PID: 0x1, once: true}),
    unmount: init(0x2, {PID: 0x1, once: true}),
    attach: init(0x4, {PID: 0x1, once: true}),
    detach: init(0x8, {PID: 0x4, KILL: 0x1, once: true}),
    error: init(0x10),
    message: init(0x20),
    multicast: init(0x40, {Observer (flush) { return {
        observe: () => { MULTICASTS.set(this, flush); },
        disconnect: () => { MULTICASTS.delete(this); }
    }}}),
    intersect: init(0x100, {Observer (flush) {
        const {root: viewport, ...options} = this.options ?? $;
        const root = targetOf.call(this.mnt, viewport);
        return new IntersectionObserver(flush, {root, ...options});
    }}),
    resize: init(0x200, {Observer (flush) {
        return new ResizeObserver(flush, this.options);
    }}),
    mutate: init(0x400, {Observer (flush) {
        const self = new MutationObserver(flush), {options} = this;
        const {observe, disconnect, takeRecords} = self;
        return fuse(self, {
            observe (target) { observe.call(self, target, options); },
            disconnect () {
                const records = takeRecords.call(self);
                disconnect.call(self), records[0] && flush(records, self);
            }
        });
    }})
}))((SID, cfg = $) => fuse({SID, emitter: Emitter(), ...cfg}));

const Supervisor = Signals.mutate.Observer
    .bind({options: {childList: true, subtree: true}}, (play => records => {
    for (const {addedNodes: attach, removedNodes: detach} of records)
        detach.forEach(play, 'detach'), attach.forEach(play, 'attach');
})(function (target) { broadcast(this, target); }));

const interrupt = (clear => target => {
    !INTERRUPTS.size && queueMicrotask(clear), INTERRUPTS.add(target);
})(() => INTERRUPTS.clear());

const broadcast = (signal, target, interrupt) => {
    if (!target || INTERRUPTS.has(target))
        return;
    interrupt?.(target), notify(signal, MOUNTS.get(target));
    for (let child = target.firstChild; child; child = child.nextSibling)
        broadcast(signal, child);
};

const notify = (signal, mnt) => {
    const {SID, PID, KILL, emitter} = Signals[signal];
    if (!mnt || mnt.SID & SID)
        return;
    (mnt.SID |= SID) & PID && mnt.OID & SID && emitter.emit(mnt, mnt.target);
    mnt.SID & KILL && unmount(mnt);
};

const emit = function (signal, mnt, ...args) {
    const {SID, emitter} = Signals[signal];
    mnt?.OID & SID && emitter.emit(this ?? mnt, ...args);
};

const raise = (e, {mnt, type, name} = TASK ?? $) => {
    if (ERRORS.has(e))
        return;
    ERRORS.add(e = e instanceof Error ? e : Error(e, {cause: e}));
    const args = [e, mnt?.context, freeze({type, name})];
    while (mnt && ERRORS.has(e))
        emit('error', mnt, ...args),
        mnt = mnt.parent ?? mntOf(mnt.context.parent);
    ERRORS.delete(e) && EMITTER.emit('error', ...args);
};

export const message = (ref, ...args) => emit('message', mntOf(ref), ...args);

export const multicast = (channel = null, ...args) =>
    MULTICASTS.forEach((flush, {channels}) =>
        channels.includes(channel) && flush(channel, ...args));

Supervisor().observe(document);

/**************************************************************  States Manager
*/
export const State = (() => {

    const find = ref => refs.get(ref) ?? fail(TypeError('invalid state'));
    const refs = new WeakMap, {emit, on, off} = Emitter({accept: find});
    let queue;

    const flush = (tasks, queue) => {
        for (const task of tasks)
            queue?.add(task) ?? task.flush();
    };

    const batch = function (resolver, ...args) {
        const tasks = queue;
        try { queue = new Set, jail(resolver, this, ...args); }
        finally { flush(queue), queue = tasks; }
    };

    const use = (key, $this, {[key]: out, value}, newval, options) =>
        isFn(out) ? jail(resolve, $this, out, newval, value, options) :
            key === 'accept' ? true : newval;

    const get = self => (TASK && TASK.out !== noop && !self.tasks.has(TASK) &&
        (self.tasks.add(TASK), TASK.states ??= []).push(self), self.value);

    const set = function (...args) {
        const [self,, options] = args, {state, value, tasks} = self;
        try {
            if (self.waits++ > intOf(options?.recursive) ||
                !use('accept', this, ...args) ||
                value === (self.value = use('digest', this, ...args)))
                return value;
            emit.call(this, state, self.value, value), flush(tasks, queue);
        } finally { self.waits--; }
        return self.value;
    };

    const State = (value, options) => {
        const {accept, digest} = options ?? $;
        const self = {state (...args) {
            return args.length ? set.call(this, self, ...args) : get(self);
        }, accept, digest, value, tasks: new Set, waits: 0};
        refs.set(self.state, self);
        return self.state;
    };

    return fuse(State, {
        owns: ref => refs.has(ref),
        peek: ref => find(ref).value,
        flush (ref) { flush(find(ref).tasks); }, batch,
        on (...args) { on.call(thisOf(State, this), ...args); }, off
    });
})();

/*********************************************************  DOM Effects Handler
*/
const Effect = (effects => {

    const flush = function () {
        const task = TASK, {out, set} = TASK = this;
        if (!this.waits++ && out !== noop)
            try {
                const value = resolve.call(this, out);
                value !== noop && set(this, value);
            } catch (e) { raise(e, this); }
        this.waits--, TASK = task;
    };

    const kill = function () {
        this.out = noop;
        for (const state of this.states)
            state.tasks.delete(this);
    };

    return (mnt, mixin, type, name, out, target = mnt.target) => {
        const task = {
            mnt, mixin, type, name, out, target, waits: 0,
            states: null, set: effects[type], flush, kill
        };
        task.flush(), task.states && join(task);
        return task;
    };
})({
    state ({mnt, mnt: {target, zombie}}, value) {
        !target ? mnt.zombie = !value : !value === !zombie && remount(mnt);
    },
    props ({target, name}, value) { target[name] = value; },
    attrs ({target, name}, value) {
        isContent(value) ? target.setAttribute(name, textOf(value)) :
            target.toggleAttribute(name, !!value);
    },
    class ({target, name}, value) { target.classList.toggle(name, !!value); },
    style ({target: {style}, name}, value) {
        value = textOf(value), name.startsWith('--')
            ? style.setProperty(name, value) : style[name] = value;
    },
    text ({target}, value) { target.nodeValue = textOf(value); },
    content ({target, target: {firstChild}}, value) {
        if (!firstChild)
            return void compile(value, target);
        if (isPrimitive(value) && firstChild.nodeType === 3 &&
            !MOUNTS.has(firstChild) && !firstChild.nextSibling)
            return void (firstChild.nodeValue = textOf(value));
        const nodes = target.childNodes, remnants = new Set(nodes);
        let patches, patch, l = (value = [value].flat(Infinity)).length;
        while (l--)
            if (remnants.delete(patch = targetOf(value[l], true)))
                (patches ??= new Map).set(patch, l).set(l, patch);
        !patches && (target.textContent = ''), remnants.forEach(detach);
        if (!patches)
            return void compile(value, target);
        for (let node, before, o = (l = value.length) - nodes.length; l--;)
            (patch = patches.get(l)) ?? (o && o--), node = nodes[l - o],
            !patch ? compile(value[l], target, node) : patch !== node &&
                (before = nodes[patches.get(node) - o + 1] ?? patch,
                move(patch, nodes[l - o + 1]), move(node, before));
    }
});

/****************************************************  Signal Observers Handler
*/
const Observe = (type => {

    const exec = function (...args) {
        const {mnt, [SYS]: {signal, action}, once} = this;
        try {
            once && free(this), resolve.call(mnt.context, action, ...args);
            signal === 'error' && ERRORS.delete(args.at(-3));
        } catch (e) { raise(e, this); }
    };

    const kill = function () {
        const {mnt, SID, emitter, observer, ref} = this;
        emitter.off(ref, this), !emitter.count(ref) && (mnt.OID &= ~SID);
        try { observer?.disconnect(); } catch (e) { raise(e, this); }
    };

    return (mnt, mixin, name, action, cfg, {nice} = $) => {
        const {priority, recursive, args, options, channels = null} = cfg;
        const {SID, emitter, Observer, once = cfg.once} = Signals[name] ?? $;
        if (!SID)
            return;
        const task = {
            mnt, mixin, type, name, [SYS]: {signal: name, action, nice},
            priority, recursive, args, options, channels: argsOf(channels),
            SID, once, emitter, observer: null, ref: null, action: exec, kill
        };
        try {
            !action && fail(TypeError('invalid action'));
            (task.observer = Observer?.call(task, emit.bind(task, name, mnt)))
                ?.observe(mnt.target);
            emitter.on(task.ref = Observer ? task : mnt, task);
            mnt.OID |= SID, join(task);
        } catch (e) { raise(e, task); }
    };
})('observe');

/*****************************************************  Event Listeners Handler
*/
const Listen = (type => {

    const handleEvent = (exec => function (event) {
        try { this.options.once && free(this), jail(exec, this, event); }
        catch (e) { raise(e, this); }
    })(function (event) {
        const {mnt: {context}, env, once, action, accept} = this;
        const args = [...this.args, event, Context((env ?? event).target)];
        if (isFn(accept) && !resolve.call(context, accept, ...args))
            return;
        const prevent = resolve.call(context, this.prevent, ...args);
        prevent & 0x1 && event.preventDefault(),
        prevent & 0x4 ? event.stopImmediatePropagation() :
        prevent & 0x2 && event.stopPropagation();
        once && free(this), resolve.call(context, action, ...args);
    });

    const kill = function () {
        const {target, name, options} = this;
        try { target.removeEventListener(name, this, options); }
        catch (e) { raise(e, this); }
    };

    return (mnt, mixin, name, action, cfg, env) => {
        const {target, options, once, args, accept, prevent} = cfg;
        const task = {
            mnt, mixin, type, name, env, target, once: options?.once,
            options: {capture: options === true, ...(options ?? $), once},
            args: argsOf(args), handleEvent, action, accept, prevent, kill
        };
        try {
            (task.target = targetOf.call(mnt, target, null, EventTarget)
                ?? mnt.target).addEventListener(name, task, task.options);
            join(task);
        } catch (e) { raise(e, task); }
    };
})('listen');

/**************************************************  Keyboard Shortcuts Handler
*/
const Keymap = (() => {

    let ID, ON, OFF;

    const modifiers = new Map([
        [1, 'altKey', 'Alt', /Alt|Option/],
        [2, 'ctrlKey', 'Control', /Control|Ctrl/],
        [4, 'metaKey', 'Meta', /Meta|Super|Command/],
        [8, 'shiftKey', 'Shift', /Shift/]
    ].map(([id, mod, key, match]) => [key, {id, mod, match}]));

    const special = new Set([
        `CapsLock NumLock ScrollLock ContextMenu Play Pause
        ArrowLeft ArrowRight ArrowDown ArrowUp Home End PageDown PageUp
        Enter Escape Tab Backspace Insert Delete`.match(/\w+/g),
        [...Array(24)].map((_, i) => `F${++i}`)
    ].flat());

    const alias = new Map([
        'Esc:Escape', 'Return:Enter', 'Space: ', 'Menu:ContextMenu',
        'Left:ArrowLeft', 'Right:ArrowRight', 'Down:ArrowDown', 'Up:ArrowUp'
    ].map(rule => rule.split(':')));

    const keyOf = ({key, code}) => !key[1] ? key.toUpperCase() :
        modifiers.has(key) ? code.replace(/Left|Right/, '') : key;

    const hook = (e, on) => e.isTrusted && !e.repeat &&
        (ID = modifiers.get(keyOf(e))?.id ?? (special.has(e.key) ? 16 : 32),
        OFF = on ? (ON |= ID, OFF || ID < 16 && ON & 48) : (ON &= ~ID) & 48);

    const test = function (cfg, args) {
        const {keys, loose, repeat, accept} = cfg, event = args.at(-2);
        if (OFF && !loose || ON < 16 || ON === 32 ||
            !event.isTrusted || event.repeat && !repeat)
            return;
        keys: for (let key of keys) {
            for (const [,{match, mod}] of modifiers)
                if (key === (key = key.replace(match, '')) !== !event[mod])
                    continue keys;
            if (keyOf(event) === (alias.get(key = key.trim()) ?? key) &&
                (!isFn(accept) || resolve.call(this, accept, ...args)))
                return true;
        }
    };

    addEventListener('keydown', e => hook(e, true), true);
    addEventListener('keyup', e => hook(e), true);

    return (mnt, mixin, {action, args, once, options, target,
        keys, loose, repeat, accept, prevent = 4}) => {
        const cfg = {keys: argsOf(keys), loose, repeat, accept};
        Listen(mnt, mixin, 'keydown', action, {accept (...args) {
            return test.call(this, cfg, args);
        }, prevent, args, once, options, target});
    };
})();

/******************************************************  Web Animations Handler
*/
const Animate = (type => {

    const ready = async task => !task.waits && (
        task.waits++, await task.target.ready, task.waits--,
        task.target.dispatchEvent(new AnimationPlaybackEvent('ready')));

    const init = ((events, listen, accept) => (task, on, env) => {
        const {target} = task, cfg = {accept, args: task, target};
        for (const [name, id] of events)
            isFn(on?.[name]) && listen(task, name, on[name], {target}, env),
            id & 1 && isFn(on?.ready) && listen(task, name, ready, cfg, env),
            id & 2 && listen(task, name, free, cfg, env);
    })(Object.entries({ready: 0, cancel: 1, finish: 1, remove: 2}),
        ({mnt, mixin}, ...args) => Listen(mnt, mixin, ...args),
        (_, e) => e.isTrusted);

    const kill = function () {
        try { this.target.cancel(), this.target.effect = null; }
        catch (e) { raise(e, this); }
    };

    return (mnt, mixin, {keyframes, on, target, options}) => {
        const task = {mnt, mixin, type, target, waits: 0, kill};
        try {
            target = targetOf.call(mnt, target, null, Element) ?? mnt.target;
            task.target = target.animate(keyframes, options);
            init(task, on, {target}), join(task);
        } catch (e) { raise(e, task); }
    };
})('animate');

/**********************************************************  Shadow DOM Handler
*/
const Shadow = ((type, name, signals) => (mnt, mixin, out) => {
    let root, {css, ...options} = (mixin ?? mnt.model).options?.shadow ?? $;
    try { root = mnt.target.attachShadow({mode: 'open', ...options}); }
    catch (e) { return void raise(e, {mnt, type, name}); }
    const cfg = {args: mnt.shadow = {root}}, env = {nice: 1};
    for (const [name, action] of signals)
        Observe(mnt, mixin, name, action, cfg, env);
    Effect(mnt, mixin, type, name, out, root);
    Effect(mnt, mixin, 'props', 'adoptedStyleSheets', css ?? [], root);
})('content', 'shadow', Object.entries({
    attach: shadow => ((shadow.observer = Supervisor()).observe(shadow.root),
        broadcast('attach', shadow.root)),
    detach: shadow => (shadow.observer?.disconnect(),
        broadcast('detach', shadow.root)),
    unmount: shadow => !shadow.observer && detach(shadow.root)
}));

/***************************************************  Controlled Inputs Handler
*/
const Input = (() => {

    const content = ({target, ictl}, value) =>
        target[ictl] === (value = textOf(value)) ? noop : value;

    const multiple = (mnt, mixin, state) =>
        Effect(mnt, mixin, 'attrs', 'multiple', isArray(State.peek(state)));

    const inputs = (ctl => fuse({
        INPUT: mnt => ctl[mnt.target.type] ?? ctl.text,
        TEXTAREA: ctl.text,
        SELECT: (modelOf => ({
            get: ({target: {selectedOptions: options}}, value) =>
                !isArray(value) ? modelOf(options[0]) :
                [...options].flatMap(option => modelOf(option) ?? []),
            set: ({target: {options}}, value) =>
                ([...options].forEach(function (option) {
                    option.selected = this.includes(modelOf(option));
                }, argsOf(value)), noop),
            use: multiple
        }))(ref => mntOf(ref)?.model)
    }))(fuse({
        text: (number => ({
            get: ({target: {type, value, min, max, step}}) =>
                !number.type(type) ? value : +((value, step) =>
                    Math.max(number.from(min, value),
                    Math.min(number.from(max, value), value)).toFixed(step))
                        (number.from(value), number.step(step)),
            set: content,
            keymap: {keys: 'Esc', action: ({target}, state) =>
                state(number.type(target.type) ? 0 : '')}
        }))({
            type: t => t === 'number' || t === 'range',
            test: n => /^[-+]?(?!00)(\d+\.?\d*|\.\d+)$/.test(`${n}`),
            from (n, o = 0) { return this.test(n) ? +n : o; },
            step (n) { return `${this.from(n)}`.split('.')[1]?.length ?? 0; }
        }),
        checkbox: {
            get: ({model, target: {checked}}, value) =>
                isBool(value) ? !!checked : (models =>
                    !!checked === models.has(model) ? value : isArray(value)
                    ? (checked ? models.add(model) : models.delete(model),
                        [...models]) : checked ? model : undefined)
                (new Set(argsOf(value))),
            set: ({model, target}, value) =>
                (target.checked = isBool(value) ? value :
                    argsOf(value).includes(model), noop)
        },
        radio: {
            get: mnt => mnt.model,
            set: ({model, target}, value) =>
                (target.checked = model === value, noop)
        },
        file: {
            get: ({target: {files}}, value) =>
                isArray(value) ? [...files] : files[0],
            set: ({target}, value) =>
                (target.files = argsOf(value).reduce((data, item) =>
                    (item instanceof File && data.items.add(item), data),
                    new DataTransfer()).files, noop),
            use: multiple
        }
    }));

    const editables = (ctl => fuse({innerHTML: ctl, textContent: ctl}))({
        get: ({target, ictl}) => target[ictl], set: content,
        use: (mnt, mixin) =>
            Effect(mnt, mixin, 'attrs', 'contenteditable', true)
    });

    const flush = (mnt, state, ctl, task, event) => {
        const value = State.peek(state);
        try { state.call(event, ctl.get(mnt, value)); }
        finally { value === State.peek(state) && task.flush(); }
    };

    const init = (mnt, mixin, state, ctl) => {
        const {set, use, keymap} = ctl = resolve(ctl, mnt);
        const out = () => set(mnt, state());
        const task = Effect(mnt, mixin, 'props', mnt.ictl, out);
        const args = [mnt, state, ctl, task];
        Listen(mnt, mixin, 'input', flush, {args, options: {capture: true}});
        keymap && Keymap(mnt, mixin, {...keymap, args, prevent: 0});
        use?.(mnt, mixin, state);
    };

    return (mnt, mixin, key, value) => {
        const {ictl, target: {tagName, namespaceURI: ns}} = mnt;
        const ctl = key === 'value' ? inputs[tagName] : editables[key];
        if (ictl || !ctl || ns !== NS.html || !State.owns(value))
            return;
        const cfg = {args: [mnt, mixin, value, ctl]}, env = {nice: 1};
        mnt.ictl = key, Observe(mnt, mixin, 'mount', init, cfg, env);
        return true;
    };
})();

/*************************************************************  Context Helpers
*/
export const Context = ref => mntOf(ref)?.context;

const ContextHandler = ((lookup, keys) => ({
    get (ctx, key) {
        const mnt = CONTEXTS.get(ctx), get = keys.get(key);
        return mnt && (get ? get(mnt) : hasOwn(ctx, key) ? ctx[key] :
            hasOwn(mnt.model, key) ? mnt.model[key] : lookup(mnt, key));
    }, ...Object.fromEntries([
        [['set', 'defineProperty', 'deleteProperty'], name =>
            (ctx, key, value) =>
                !keys.has(key) ? Reflect[name](ctx, key, value) :
                fail(TypeError(`context "${key}" is read-only`))],
        [['preventExtensions', 'setPrototypeOf'], () => () =>
            fail(TypeError('context is not configurable'))]
    ].flatMap(([names, fn]) => names.map(name => [name, fn(name)])))
}))((mnt, key) => (mnt.parent?.context ?? Context(mnt.context.parent))?.[key],
    new Map([...`ns is id props attrs class style content shadow
            observe listen keymap animate mixins options state init`
        .match(/\w+/g).map(key => [key, mnt => mnt.model[key]]),
    ['parent', mnt => (mnt.parent ?? Context(mnt.target?.parentNode))?.model],
    ...['root', 'model', 'target'].map(key => [key, mnt => mnt[key]])]));

/************************************************************  Mounting Helpers
*/
const mount = ((bind, kill) => (model, mnt) => {
    const {
        ns, is, id, props, attrs, class: clist, style, content, shadow,
        observe, listen, keymap, animate, mixins, options, state, init
    } = model, mixin = mnt ? model : null;
    if (!mnt) {
        if (MOUNTS.has(model))
            return void raise(Error('model is active', {cause: model}));
        const parent = TASK?.type === 'content' ? TASK.mnt : MNT;
        if (parent && !parent.target)
            return void raise(Error('mount not ready', {cause: model}));
        const root = Component.owns(model) ? model : parent?.root;
        const ctx = {}, context = new Proxy(ctx, ContextHandler);
        CONTEXTS.set(ctx, mnt = MNT = {
            root, parent, model, target: null, shadow: null, type: null,
            MNT, ctx, context, tasks: null, mixins: null, evolve: null,
            SID: 0, OID: 0, zombie: isFn(state), ictl: null, time: null
        }), MOUNTS.set(model, mnt);
        try { isFn(init) && jail(init, context); }
        catch (e) { return void kill(mnt, 'mount::init', e); }
        mnt.zombie && Effect(mnt, null, 'state', null, state);
        const tag = mnt.zombie ? '#comment' : is ?? 'div';
        const tns = NS[ns] ?? ns ?? NS[tag] ?? parent?.target.namespaceURI;
        try { MOUNTS.set(mnt.target = create(tag, tns, options?.is), mnt); }
        catch (e) { return void kill(mnt, 'mount::host', e); }
        mnt.type = mnt.target.nodeType === 1 ? 'content' : 'text';
    }
    if (!mnt.zombie) {
        observe && bind.events(mnt, mixin, observe, Observe);
        listen && bind.events(mnt, mixin, listen, Listen);
        keymap && bind.extras(mnt, mixin, keymap, Keymap);
        animate && bind.extras(mnt, mixin, animate, Animate);
        !mixin && id && isText(id) && (mnt.target.id = id);
        props && bind.props(mnt, mixin, props);
        isContent(content) && Effect(mnt, mixin, mnt.type, null, content);
        if (mnt.type === 'content')
            isContent(shadow) && Shadow(mnt, mixin, shadow),
            attrs && bind.attrs(mnt, mixin, 'attrs', attrs),
            clist && bind.attrs(mnt, mixin, 'class', clist),
            style && bind.attrs(mnt, mixin, 'style', style);
        !mixin && bind.mixins(mnt, mixins);
    }
    !mixin && ({MNT} = mnt, notify('mount', mnt));
    return mnt.target;
})({
    props (mnt, mixin, cfg) {
        for (const key in cfg)
            if (hasOwn(cfg, key) && !Input(mnt, mixin, key, cfg[key]))
                Effect(mnt, mixin, 'props', key, cfg[key]);
    },
    attrs (mnt, mixin, type, cfg) {
        if (type !== 'attrs' && !isObject(cfg))
            return void Effect(mnt, mixin, 'attrs', type, cfg);
        for (const key in cfg)
            hasOwn(cfg, key) && Effect(mnt, mixin, type, key, cfg[key]);
    },
    events (mnt, mixin, cfg, set) {
        for (const ref of argsOf(cfg))
            for (const type in (ref ?? $))
                for (const handler of argsOf(ref[type]))
                    handler && hasOwn(ref, type) && set(mnt, mixin, type,
                        fnOf(handler) ?? fnOf(handler.action), handler);
    },
    extras (mnt, mixin, cfg, set) {
        for (const ref of argsOf(cfg))
            ref && set(mnt, mixin, ref);
    },
    mixins (mnt, mixins) {
        if (isArray(mixins))
            for (const mixin of (mnt.mixins = new Set(mixins)))
                isObject(mixin) && mount(mixin, mnt);
        if (mixins = MIXINS.get(mnt.model))
            for (const [mixin, options] of [...mixins])
                !mnt.mixins?.has(mixin) && mount(mixin, mnt),
                options.once && mixins.delete(mixin);
        mnt.evolve = true;
    }
}, (mnt, type, e) => ({MNT} = mnt, raise(e, {mnt, type}), unmount(mnt)));

const remount = mnt => {
    const {model, target, target: {parentNode}} = mnt;
    const marker = parentNode.insertBefore(create('#comment'), target);
    detach(target), compile(model, parentNode, marker), marker.remove();
};

const unmount = (free => mnt => {
    const {ctx, model, target, tasks} = mnt;
    MOUNTS.delete(model), MOUNTS.delete(target), tasks?.forEach(free);
    notify('unmount', mnt), CONTEXTS.delete(ctx);
})(task => task[SYS]?.signal !== 'unmount' && free(task));

export const evolve = (model, mixin, options) => {
    const mnt = mntOf(model), mixins = MIXINS.get(model) ?? new Map;
    if (!mixins.has(mixin) && isObject(model) && isObject(mixin))
        MIXINS.set(model, mixins.set(mixin, {once: options?.once})),
        mnt?.evolve && !mnt.mixins?.has(mixin) && mount(mixin, mnt);
};

export const revoke = (free => (model, mixin, options) => {
    const mnt = mntOf(model), mixins = MIXINS.get(model);
    if (mixins?.delete(mixin))
        !mixins.size && MIXINS.delete(model), mnt?.evolve &&
        (options?.remount ? remount(mnt) : mnt.tasks?.forEach(free, mixin));
})(function (task) { this === task.mixin && free(task); });

/**************************************************************  System Helpers
*/
const vacuum = options => freeze([...MOUNTS].filter(([ref, mnt]) =>
    ref === mnt.model && mnt.target && !mnt.target.isConnected &&
    intOf(ref.options?.ttl ?? options?.age ?? 60000) <
    (Date.now() - (mnt.time ??= Date.now()))).map(([model, mnt]) =>
        (!options?.analyze && unmount(mnt), model)));

export default fuse({VERSION, on, off, vacuum, noop});
