<p align="center">
    <a href="https://karyon.dev">
        <img src="https://karyon.dev/img/logo-full.svg" alt="KARYON">
    </a>
</p>

A [JavaScript module](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)
for creating highly composable, reactivity-driven, high-performance
web application interfaces. It provides convenient and efficient means of
abstraction from the imperative
[DOM API](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model),
facilitating the development at various levels of complexity.

## Features

- Freedom of creativity by means of flexible declarative model,
    rich functionality and powerful composition capabilities.
- Instant reactivity engaging only dynamic parts of UI,
    through real-time tracking and efficient granular updates.
- Pure JavaScript, free of syntax extensions and interspersed markup,
    for unstrained, unconstrained developer experience.

## Quickstart

Check out [the documentation](https://karyon.dev/handbook/)
and [interactive examples](https://karyon.dev/workbench/)
at [KARYON website](https://karyon.dev).
